##################
EQEmu Login Helper
##################

Clientside workaround for the lack of out-of-order packet handling in the public EQEmu loginserver.

This is a clone of Zaela's eqemu login helper, modified for RoF2, port 5999 for Legacy of Norrath (LoN).

Original (credit to Zaela), here: https://github.com/Zaela/eqemu-login-helper 

*****
Usage
*****

* Change the Host eqhost.txt file inside the LoN client directory to localhost:5999
* Start lon-eqemu-login-helper and keep it running in the background
* Login as normal

